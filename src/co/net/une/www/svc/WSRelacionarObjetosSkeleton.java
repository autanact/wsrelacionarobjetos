
/**
 * WSRelacionarObjetosSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WSRelacionarObjetosSkeleton java skeleton for the axisService
     */
    public class WSRelacionarObjetosSkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WSRelacionarObjetosLocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param dataset
                                     * @param codigoBase
                                     * @param tablaBase
                                     * @param codigoRelacionado
                                     * @param tablaRelacionada
         */
        

                 public co.net.une.www.gis.GisRespuestaGeneralType_GDE relacionarObjetos
                  (
                  java.lang.String dataset,java.lang.String codigoBase,java.lang.String tablaBase,java.lang.String codigoRelacionado,java.lang.String tablaRelacionada
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("dataset",dataset);params.put("codigoBase",codigoBase);params.put("tablaBase",tablaBase);params.put("codigoRelacionado",codigoRelacionado);params.put("tablaRelacionada",tablaRelacionada);
		try{
		
			return (co.net.une.www.gis.GisRespuestaGeneralType_GDE)
			this.makeStructuredRequest(serviceName, "relacionarObjetos", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    